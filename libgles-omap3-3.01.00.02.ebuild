# Copyright 2010 Neuvoo Developers
# Distributed under the terms of the GNU General Public License v2
# $Header: $

inherit versionator

SDKPV=${PV}
SDK_PV=$(replace_all_version_seperator '_' ${SDKPV})
SGXMODPV="1.4.14.2514"
