# Copyright 2010 Neuvoo Developers
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI="2"

inherit versionator linux-mod

SDKPV="3.01.00.02"
SDK_PV=$(replace_all_version_separators '_' ${SDKPV})
SDK_PN="OMAP35x_Graphics_SDK"
SDKFILENAME="${SDK_PN}_setuplinux_${SDK_PV}.bin"

DESCRIPTION="Kernel drivers for the PowerVR SGX chipset found in the omap3 SoCs"
HOMEPAGE="http://wiki.davincidsp.com/index.php/OMAP35x_Graphics_SDK_Getting_Started_Guide"
SRC_URI="omap3-sgx-modules-${PV}.tar.bz2"
SRC_URI_TI="http://software-dl.ti.com/dsps/forms/export.html?prod_no=/${SDKFILENAME}"

RESTRICT="fetch"
LICENSE="GPL-2"
KEYWORDS="~x86 ~arm"
SLOT="0"
IUSE=""
DEPEND=""

SSUB="${SDK_PN}-${SDKPV}"

MODULE_NAMES="omaplfb(kernel/drivers/gpu/pvr:${S}:${S})"
BUILD_TARGETS="clean all"
BUILD_PARAMS="KERNELDIR=${KERNEL_DIR}"

pkg_nofetch() {
	einfo "To obtain this file you need a my.TI account, so please register"
	einfo "Furthermore the software requires US Government export approval"
	einfo "before downloading..."
	einfo "Due to freuqent changes on TI's website the following link may"
	einfo "have changed in the meantime:"
	einfo "${SRC_URI_TI}"
	einfo "Download ${SDKFILENAME} and run the ti-sgx-unpack script with option '-k'."
	einfo "Place the generated ${P}.tar.bz2 in ${DISTDIR}"
}


src_prepare() {
	epatch "${FILESDIR}/0001-Compile-fixes-for-recent-kernels.patch"
}

pkg_postinst() {
	linux-mod_pkg_postinst
	ewarn ">> This is not a official Gentoo ebuild <<"
	ewarn "Please use bugtracker at http://neuvoo.org/bugs"
	ewarn "or contanct neuvoo devs by IRC in #neuvoo on freenode"
}
