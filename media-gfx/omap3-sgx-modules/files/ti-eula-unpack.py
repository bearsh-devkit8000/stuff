# -*- coding: utf-8 -*-
# Copyright 2010 Neuvoo Developers
# Copyright 2009-2010 OpenEmbembedded
# version 0.2
#
# This file defines function used for unpacking the .bin file downloaded over
# the http and display EULA.
#
# usage:
# python ti-eula-unpack.py BINFILE TARFILE TI_BIN_UNPK_CMDS [TI_BIN_UNPK_WDEXT]
#
#   BINFILE           - name of the install jammer .bin file (absolute or relative path)
#   TARFILE           - name of the tar file inside the install jammer if any
#   TI_BIN_UNPK_CMDS  - contains list of commands separated with colon to be 
#                       passed while unpacking the bin file. The keyword 
#                       workdir expands to WORKDIR and commands are appendded
#                       with '\n'. Eg. TI_BIN_UNPK_CMDS="Y:Y: qY:workdir"
#   TI_BIN_UNPK_WDEXT - This variable extends workdir path, if user wants to put
#                       the output in some internal directory


import os, sys, time

if len(sys.argv) < 4 and len(sys.argv) > 5:
    print "wrong number of arguments"
    print "ti-eula-unpack.py BINFILE TARFILE TI_BIN_UNPK_CMDS [TI_BIN_UNPK_WDEXT]"
    sys.exit(1)

binfile = sys.argv[1]
tarfile = sys.argv[2]
ti_bin_unpk_cmds = sys.argv[3]
ti_bin_unpk_wdext = False
try:
    ti_bin_unpk_wdext = sys.argv[4]
except IndexError:
    pass

if not os.path.exists(binfile):
    print binfile + " does not exist..."
    sys.exit(1)

workdir = os.getcwd()
binfile = os.path.abspath(binfile)
cmd_list = ti_bin_unpk_cmds.split(":")
instdir = workdir
if ti_bin_unpk_wdext:
    instdir = os.path.join(workdir, ti_bin_unpk_wdext)

# Make the InstallJammer binary executable so we can run it
os.chmod(binfile, 0755)

# Run the InstallJammer binary and accept the EULA
filename = "HOME=%s %s" % (workdir, binfile)
print filename
f = os.popen(filename,'w',1)
for cmd in cmd_list:
    if cmd == "workdir":
        cmd = instdir
    print >>f, "%s" % cmd
f.close()

# Expand the tarball that was created if required
if bool(tarfile) == True:
    tarfile  = os.path.join(workdir, tarfile)
    tcmd = 'tar xz --no-same-owner -f %s -C %s' % (tarfile, workdir)
    os.system(tcmd)

sys.exit(0)
